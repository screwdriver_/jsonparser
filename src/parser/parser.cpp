#include <json.hpp>

using namespace JSON;

template<typename T>
static inline std::shared_ptr<T> ptr()
{
	return std::shared_ptr<T>(new T);
}

static Element_t parse_value(const char *&str)
{
	auto val = ptr<Value>();

	skip_spaces(str);
	if(is_null(str))
		str += 4;
	else if(is_float(str))
		*val = get_float(str);
	else if(is_integer(str))
		*val = get_integer(str);
	else if(is_boolean(str))
		*val = get_boolean(str);
	else if(is_string(str))
		*val = get_string(str);
	else
		throw std::runtime_error("Valid value expected!");
	return val;
}

static Element_t parse_object(const char *&str)
{
	skip_spaces(str);
	if(*str != '{')
		expect('{');
	++str;

	auto obj = ptr<Object>();
	while(*str != '}')
	{
		skip_spaces(str);
		if(*str == '}')
			break;
		if(*str == '\0')
			ueod();

		const auto name = get_string(str);
		skip_spaces(str);
		if(*str != ':')
			expect(':');
		++str;
		skip_spaces(str);
		(*obj)[name] = parse(str);
		skip_spaces(str);
		if(*str == ',')
		{
			++str;
			continue;
		}
		skip_spaces(str);
		if(*str != '}')
			expect('}');
	}
	++str;
	return obj;
}

static Element_t parse_array(const char *&str)
{
	skip_spaces(str);
	if(*str != '[')
		expect('[');
	++str;

	auto arr = ptr<Array>();
	while(*str != ']')
	{
		skip_spaces(str);
		if(*str == ']')
			break;
		if(*str == '\0')
			ueod();
		arr->push(parse(str));
		skip_spaces(str);
		if(*str == ',')
		{
			++str;
			continue;
		}
		skip_spaces(str);
		if(*str != ']')
			expect(']');
	}
	++str;
	return arr;
}

Element_t JSON::parse(const char *&str)
{
	skip_spaces(str);
	switch(*str)
	{
		case '{': return parse_object(str);
		case '[': return parse_array(str);
		default: return parse_value(str);
	}
}
