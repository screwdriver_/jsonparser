#ifndef VALUE_TPP
# define VALUE_TPP

namespace JSON
{
	template<>
	inline Value::operator const char*() const
	{
		return reinterpret_cast<const char*>(data);
	}

	template<>
	inline void Value::operator=(const int v)
	{
		store(&v, sizeof(v));
		type = INTEGER_JT;
	}

	template<>
	inline void Value::operator=(const float v)
	{
		store(&v, sizeof(v));
		type = FLOAT_JT;
	}

	template<>
	inline void Value::operator=(const char* v)
	{
		if(v == nullptr) {
			null();
			return;
		}

		store(v, strlen(v) + 1);
		type = STRING_JT;
	}

	template<>
	inline void Value::operator=(const std::string v)
	{
		operator=(v.c_str());
	}

	template<>
	inline void Value::operator=(const bool v)
	{
		store(&v, sizeof(v));
		type = BOOLEAN_JT;
	}
}

#endif
