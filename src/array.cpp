#include <json.hpp>

using namespace JSON;

std::string Array::dump(const bool pretty, const size_t level) const
{
	if(elements.empty())
		return "[]";
	std::string str = "[";
	if(pretty)
		str += '\n';
	for(auto i = elements.cbegin(); i != elements.cend(); ++i)
	{
		if(pretty)
			append_tab(str, level + 1);
		str += (*i)->dump(pretty, level + 1);
		if(next(i) != elements.cend())
			str += ',';
		if(pretty)
			str += '\n';
	}
	if(pretty)
		append_tab(str, level);
	str += ']';
	return str;
}
