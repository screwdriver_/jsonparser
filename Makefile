NAME = JSON.a
CPPC = g++
CPPFLAGS = -Wall -Wextra -Werror -std=c++17 -O3 -fPIC

SRC_DIR = src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDR := $(shell find $(SRC_DIR) -type f -name "*.hpp" -or -name "*.tpp")
DIRS := $(shell find $(SRC_DIR) -type d)

OBJ_DIR = obj/
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(DIRS))
OBJ := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRC))

LIBS = -lstdc++

all: $(NAME) tags

$(NAME): $(OBJ_DIRS) $(OBJ)
	ar rc $(NAME) $(OBJ)

$(OBJ_DIRS):
	mkdir -p $(OBJ_DIRS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDR)
	$(CPPC) $(CPPFLAGS) -I $(SRC_DIR) -c $< -o $@ $(LIBS)

tags: $(SRC) $(HDR)
	ctags -R $(SRC) --languages=c,c++

clean:
	rm -rf $(OBJ_DIR)

fclean: clean
	rm -f $(NAME)
	rm -f tags

re: fclean all

.PHONY: all mkdir clean fclean re
